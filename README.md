```

```

This repository will be used to configure a stable, scalable easy to use Nix 
environment, deployed on multiple Raspberry Pi 3 devices.

# Based on:

- [NixOS for Raspberry Pi](https://nixos.wiki/wiki/NixOS_on_ARM/Raspberry_Pi)
- [NixOS Configuration Collection](https://nixos.wiki/wiki/Configuration_Collection)
- [Brainrape's tutorial](https://github.com/brainrape/nixos-tutorial)

# Official Documentation
Wiki for Nix can be found in:
- [NixOS wiki](https://nixos.wiki/wiki/NixOS)

# Changelog

| date       | Event               |
|------------|---------------------|
| 2020-03-04 | Initial commit      |
| yyyy-mm-dd | whatever did happen |