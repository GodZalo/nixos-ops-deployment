{ config, pkgs, lib, ... }:

{
  # mount tmpfs on /tmp
  boot.tmpOnTmpfs = lib.mkDefault false;

  # install basic packages
  environment.systemPackages = [
    #vim
    #pkgs.firefox
    pkgs.qutebrowser
    pkgs.sipcmd
    pkgs.pjsip
    #htop
    #wget
    #curl
  ];

  programs.bash.enableCompletion = true;

  # copy the system configuration into nix-store
  system.copySystemConfiguration = true;
}
