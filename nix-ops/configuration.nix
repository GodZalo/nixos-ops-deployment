# Testing nix configuration file
# further research required
# Work in progress

{ config, pkgs, lib, ... }:
{
  imports =
    [
      #Include the results of the hardware scan.
      ./hardware-configuration.nix

      # Adds user profiles
      ./profiles.nix

      ./common.nix
    ];
  documentation = {
    dev.enable = false;
    man.enable = false;
    nixos.enable = false;
  };

  # Set Time Zone
  time.timeZone = "America/Bogota";
  services = {
    timesyncd.enable = true;
    ntp.enable = true;

    # Enable the OpenSSH server.
    sshd.enable = true;
    openssh.enable = true;

    xserver = {
      videoDrivers = ["modesetting"];
      layout = "us";
      enable = true;
      useGlamor = true;
      desktopManager.xterm.enable = true;
      windowManager = {
        awesome.enable = true;
        default = "awesome";
      };
    };
  };

  # Keep the latest kernel available (5.4 rn).
  boot.kernelPackages = pkgs.linuxPackages_latest;

  boot ={
    kernelParams = ["cma=256M"];
    loader = {
      # Use the extlinux boot loader. NixOS wants to enable GRUB by default
      grub.enable = false;
      # Enables the generation of /boot/extlinux/extlinux.conf
      generic-extlinux-compatible.enable = true;

      raspberryPi = {
        enable = true;
        version = 3;
        uboot.enable = true;
        uboot.configurationLimit = 1;
        firmwareConfig = ''
          gpu_mem=256
        '';
      };
    };
  };
  # !!! Adding a swap file is optional, but strongly recommended!
  swapDevices = [ { device = "/swapfile"; size = 2048; } ];

  nixpkgs.config = {
    allowUnfree = true;
    allowUnsupportedSystem = true;
  };

  networking.wireless = {
    enable = true;
    networks = {
      "Don Barredora" = {
        psk = "12341234";
      };
  };
  };

  system.stateVersion = "19.09";
}