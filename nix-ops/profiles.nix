{ config, pkgs, lib, ... }:
{
	users.users.root = {
		password = "this";
		openssh.authorizedKeys.keys = [ "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDrAwsJdLJHkq8pMi5Yyx23kP3M08uT6l+jDqKGhzpX35GFLTKf0k/M9nJkhN32iQfoyTwOkwFb8EG1HA5h55z0HrogxV0QlJdens2TiU+b1xMQATakIA118UNS6V+29A80/LzVShYCue/au+JsvfHRBdaWnReX2QWYtEbCkzk0yAqQ+P/4OjuuOSnsNyEf+lPJjDbPZsqnnyC1bDM6ZnRMdSi0PTDFf+gNrjvrQ9XxVcEYo8OLF3i3Qg6RQl9zqO9UWBSc0YJ7ZF8nH1Wl/89HgZIJhg/GRVkMSdldlSmEl8w2sj88wr8k/YABHXYyABrkza44VsV0Zr3bdYBtNjb702/9peXGMIemeEzdRxQyvX8x/1MQ/v1O/7/KOMSViq8zX0qyjqlMR4xGbn4BcZ8yJi32Td5cF3Pij9xVgEgNdeoSTQTVTrFyzMjIg6TG5+jSOpIE2UsMOlgprTu22635pd5mmFhmfgI6GthHObwHC55qdQgw7xp0RO6NQjWV7A45fb1lBI+JWZjopc6xy8e4BPPdKIq7lKOudY+H/IhCFbC22XTPGIYj4DdIPVXFk2bLEOZZkokqJ4lUOgqk6ymNLvGkpN/ZUd1tjQwwyALFv4HN7L0+p2aTm5RHlhvtTex/JBXdaqd5uNkJEy/WYRpLoogi9CrJHx351bQIGRqs1w== nma@xoe.solutions" "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC0abNe6KpUYhijzZQ3hjKeNnc+bvrP4FEp7aOjBiWHREKhkr12EfzcHb9fr6Eo5Drdjfsh+wgvJgXVjM8+d5a1V3TxLaZ57wWQaXkb0R+s48Kahmyl9G2q7NFfOz9zEARKTyK5I/dvBz9oTFC6Zcq4FvY9YfFs/GfWw3IQZ9IDhOt/Zrrr/nPp4amjJHhG5NlAhORroN/pft1PmRiykM/1QXtstPNbrkl6AOAWfPeob5EJgDBYTWRnvQ+kulAiu1aTQ+jGh2s8/TRB0odETomupptzjLRN3ZZyFT9g11B5Sl/pe/SWv6Ohdev0AWharDqObe6c4hChOcMXvAmVJkw6F9/2qZSU0cwihz6S7KLdIec2nH2Qvphth/wRItMmGKsD60Q6w+4/JMj1Tn6x/aEMsXBR9rFhCjtMM4JXsMIg9N4+m0xsGGp6+JoZAMciGYYJANSmdZOWfzmcBg1oDzOIC+S+FWPVwnornw9ajloPNbCAjQO/1TRqAaVOaJLuz2E= nma@xoe.solutions" ];
  };

	users.users.admin = {
		isNormalUser = true;
		extraGroups = [ "wheel" ]; # Enable `sudo` for the user.
		password = "test";
  };

	users.users.agente1 = {
		isNormalUser = true;
		password = "test";
  };

	users.users.agente2 = {
		isNormalUser = true;
		password = "test";
  };
  
	users.users.agente3 = {
		isNormalUser = true;
		password = "test";
  };

}