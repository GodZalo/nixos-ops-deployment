# Prerequisites:
- We used [this image](https://hydra.nixos.org/build/113291926/download/1/nixos-sd-image-19.09.2129.9fef2ce7cfb-aarch64-linux.img)
using kernel 5.4 and NixOS 19.09
- Have booted at least once
- Have a somewhat decent internet connection
- timesync services are running (`systemctl status time-sync.target`)
- Time is somewhat updated (or else, SSL certs will fail upon each curl action)

# Where to start
I'm assuming you already have the image already burnt in the SD card, and 
booted at least once, and time is updated.

Getting the base to a funcional NixOS can be achieved by following the next
steps.

<!--

- First we need to add the NixOS 20.03 channel which can be made running
`nix-channel --add https://nixos.org/channels/nixos-20.03`
- Now `nix-channel --update` to make effective channel.

-->

- Run `nixos-generate-config` to create default `configuration.nix`
- Run `nixos-collect-garbage -d` to delete old symlinks and stuff.
- Run `nixos-rebuild switch` to make the new build.
- If build was successful, you can proceed to make a `reboot now`

# Where to go next [WIP]

## Setting base environment
- Make sure timesync is still working properly with the time synced!
- Get the updated `configuration.nix` by running `curl https://gitlab.com/GodZalo/nixos-ops-deployment/-/raw/master/nix-ops/configuration.nix > ~/../etc/nixos/configuration.nix`
- Do the same with `common.nix` and `profiles.nix`
- Run `nixos-rebuild switch` to apply changes
- If successful `reboot`

At this point you should be able to log in under any of the users at 
`/profiles.nix` trough the window manager.

If you need to access the console at tty1 press `ctrl + alt + F1` and do as
you please.

## Installing packages

<!-- No longer needed as of common.nix tmp solves this, keeping documentation
	in case it's needed

At this point, it's most likely that you are unable to install any pkg via 
`nix-env -iA <pkg>` or even at the `common.nix` examples. The reason why this 
happens, is related to the message `No space left on device`, but to be more
precise, it's due to `inodes` allocation. By default NixOS creates a temporary 
`/temp` partition which is half the size of the RAM (in this case for the rpi
3b, 512Mb), which contains something around 110k `inodes` which will not 
suffice any .tar package to be decompressed, in the case of `pkg.firefox` will
use around 3 ~ 4 Gb of space and around 200k inodes (may vary).

As it's not possible to get more `inodes` on an easy and conventional way, I
(without knowing if this is te appropiate solution) opted for unmounting 
manually `/tmp` and disabling the automatic `tmp.mount` service and then
proceding to install my desired package.

- `umount -l /tmp`
- `systemctl stop tmp.mount`
- `nix-env -iA qutebrowser`

If succesful (and ignoring the tmp warning messages) `reboot`, and after boot
you can simply run on a terminal (after user login via Desktop Manager) 
`qutebrowser` and you should be able to access internet. 
 -->

Just set up using the examples on `profiles.nix` or via `nix-env -iA <pkg>`

In example the installation of qutebrowser was made running
- `nix-env -iA qutebrowser`
- And can be lauched post Window Manager login in any terminal by using
  `qutebrowser` (warnings can be ignored).

## Configuring softphone [WIP]

- ~~Linphone is available for NixOS~~ (Does not compile)
- sipcmd (command line sip client, testing, so far so good) 


## Setting up Vicidial [WIP]

- Maybe in a server like Kuma


# Changelog

## V.0.3
- Added packages installation guide.

## V.0.2
- Removed `man` documentation, unneeded, high RAM comsumption while building
new generations via `nixos-rebuild switch`
- Enabled DesktopManager and `awesome` WindowManager (other windowmanagers are 
being tested).

## V.0.1
- Final Where to start section documentated
- Added a limit to maximum build generations available at boot
- Fixed timesync and time zone error
- Added root user auto login
- Extended (doubled) swap memory available